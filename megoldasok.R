set.seed(112)
library(randomForest)

#1. feladat
#1.1 adatbeolvasas

fbDF <- as.data.frame(read.csv(file = 'pageData_HumansOfNewYork.csv',
                               header = T,
                               colClasses = c(rep('factor',2), #from_id, from_name
                                              'character', #message
                                              'character', #created_time, kesobb Date
                                              'factor',# type
                                              'character', #link
                                              'character', #id
                                              rep('numeric',3) #likes_count, comments_count, shares_count
                                              ),
                               encoding = 'UTF-8'
                               )
                      )

#1.2 
colnames(fbDF) #oszlopnevek
dim(fbDF)[1] #sorok szama

length(levels(fbDF$type)) #type csoportok
levels(fbDF$type) #ezek szamossaga

png('post_types.png',720,620)
plot(fbDF$type, main = 'Distribution of post types', ylab='Count')
dev.off()

summary(fbDF$likes_count) #like leiroi

png('like_count_hist.png',720,620)
hist(fbDF$likes_count, 
     main='Histogram of like_count variable', 
     col=rgb(139,157,195,255, maxColorValue = 255), 
     xlab='number of likes',
     breaks = 25
    )
dev.off()

sum(head(fbDF$likes_count,100)) #legutobbi 100 osszesitve

sum(!is.na(fbDF$link)) #linket tartalmazo postok szama

aggregate(fbDF$link ~ fbDF$type, fbDF, length) #linkek a kulonbozo tipusokhoz

fbDF[which(fbDF$likes_count >= 10 
           & fbDF$type == 'status' 
           & fbDF$comments_count == min(fbDF$comments_count)),c("id","comments_count")]

#2. feladat
#2.1 ujak szurese

getNumericTimeFromCreatedTimeField <- function(created_times){
  if (
    is.null
    (created_times)){
    NULL
  } else {
    as.numeric
    (
      strptime
      (created_times,format="%Y-%m-%dT%H:%M:%S"))
  }
}

fbDF$created_time <- 
  getNumericTimeFromCreatedTimeField(fbDF$created_time) #konvertalas

attributes(fbDF$created_time) #milyen attributumok erhetoek el

date_compare_to <- fbDF[1,"created_time"] #viszonyitasi/legujabb datum

fbDF_older <- fbDF[-which(fbDF$created_time$yday > date_compare_to$yday - 5 &
                           fbDF$created_time$year == date_compare_to$year &
                           fbDF$created_time$yday <= date_compare_to$yday),]

#2.2 train-test

testDF <- fbDF_older[1:round(dim(fbDF_older)[1]*0.3),] #legujabb 30% test
trainDF <- fbDF_older[-(1:round(dim(fbDF_older)[1]*0.3)),] #legkorabbi 70% train 

#3. feladat
#eltelt napok szama
time_diff <- function(inDF){
  date_compare_to <- inDF[1,"created_time"]
  inDF$elapsed_days <- as.integer(difftime(date_compare_to,
                                           inDF[, "created_time"],
                                           units = "days"))
  return(inDF)
}

#het napja
week_day <- function(inDF){
  inDF$day_of_week <- as.factor(weekdays(as.Date(inDF$created_time)))
  return(inDF)
}

#message hossza
msg_len <- function(inDF){
  inDF[which(is.na(inDF$message)), "message"] <- ""
  
  inDF$msg_len <- nchar(inDF$message)
  return(inDF)
}

#napszak oraban
time_of_day <- function(inDF){
  inDF$time_of_day <- ifelse(inDF$created_time$hour < 6, "ejjel",
                        ifelse(inDF$created_time$hour < 12, "reggel",
                          ifelse(inDF$created_time$hour < 18, "delutan", 
                            "este"
                            )
                          )
                        )

  
  inDF$time_of_day <- as.factor(inDF$time_of_day)
  return(inDF)
}

#van-e link
link_flag <- function(inDF){
  
  inDF$link_flag <- as.factor(!is.na(inDF$link))
  return(inDF)
}

runfunctions <- function(inDF, func_vector){
  for( func in func_vector)
    inDF <- eval(call(func,inDF))
  return(inDF)
}

#most mar nem kellenek, hogy megirtam a runfunctionst, es leteszteltem, hogy ugyan azokat kapom-e
# testDF2 <- testDF
# trainDF2 <- trainDF

#mind az ot fuggveny meghivasa a ket DF-en
# testDF <- time_diff(testDF)
# testDF <- week_day(testDF)
# testDF <- msg_len(testDF)
# testDF <- time_of_day(testDF)
# testDF <- link_flag(testDF)
# 
# trainDF <- time_diff(trainDF)
# trainDF <- week_day(trainDF)
# trainDF <- msg_len(trainDF)
# trainDF <- time_of_day(trainDF)
# trainDF <- link_flag(trainDF)

#ez igy szebb
functionVector <- c("time_diff","week_day","msg_len","time_of_day","link_flag")
#most a rovidebb hivassal mar ezek a kettes utotagok sem fognak kelleni, 
#majd a fuggvenyneveket kitorlom, hogy ne legyen utban foloslegesen
#testDF2  <- runfunctions(testDF2,functionVector)
#trainDF2 <- runfunctions(trainDF2,functionVector)
testDF  <- runfunctions(testDF,functionVector)
trainDF <- runfunctions(trainDF,functionVector)
rm(functionVector)

#biztos, hogy jol csinaltam?
# all.equal(testDF2,testDF)
# all.equal(trainDF2,trainDF)

#4. feladat
#Modellezés

# "elapsed_days" "day_of_week"  "msg_len" "time_of_day" "link_flag" 
independent_variables <- colnames(trainDF)[c(11,12,13,14,5)]
f <- as.formula(paste0('likes_count ~ ',paste0(independent_variables,collapse = '+')))

#4.1 - 4.2
#train
linear_modell <- lm(formula = f, data = trainDF)
rf_modell <- randomForest(formula = f, data = trainDF)
#test
testDF$predict_lm <- predict(linear_modell,testDF)
testDF$predict_rf  <- predict(rf_modell,testDF)

#4.3
sapply(testDF[,c(11,12,13,14,5)],class)
sapply(testDF[,c(11,12,13,14,5)],typeof)

summary(linear_modell)

#5. feladat

testDF$predict_mean <- mean(trainDF$likes_count)

mse <- function(predictions,realValues){
  return(1/(length(predictions)) * sum(
                                      (realValues - predictions)*
                                      (realValues - predictions)
                                    )
  )
}

cat(' Mean squared error of the linear modell: \t\t',mse(testDF$predict_lm,testDF$likes_count),'\n',
    'Mean squared error of the random forest modell: \t',mse(testDF$predict_rf,testDF$likes_count),'\n',
    'Mean squared error of the basic mean prediction: \t',mse(testDF$predict_mean,testDF$likes_count),'\n')
